def check_httpd_service(host):
    """Check that the httpd service is running on the host"""
    assert host.service("httpd").is_running